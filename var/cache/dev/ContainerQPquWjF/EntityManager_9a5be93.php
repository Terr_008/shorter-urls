<?php

namespace ContainerQPquWjF;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder9ac7f = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer23f97 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties58d49 = [
        
    ];

    public function getConnection()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getConnection', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getMetadataFactory', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getExpressionBuilder', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'beginTransaction', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getCache', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getCache();
    }

    public function transactional($func)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'transactional', array('func' => $func), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'wrapInTransaction', array('func' => $func), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'commit', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->commit();
    }

    public function rollback()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'rollback', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getClassMetadata', array('className' => $className), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'createQuery', array('dql' => $dql), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'createNamedQuery', array('name' => $name), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'createQueryBuilder', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'flush', array('entity' => $entity), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'clear', array('entityName' => $entityName), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->clear($entityName);
    }

    public function close()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'close', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->close();
    }

    public function persist($entity)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'persist', array('entity' => $entity), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'remove', array('entity' => $entity), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'refresh', array('entity' => $entity), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'detach', array('entity' => $entity), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'merge', array('entity' => $entity), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getRepository', array('entityName' => $entityName), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'contains', array('entity' => $entity), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getEventManager', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getConfiguration', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'isOpen', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getUnitOfWork', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getProxyFactory', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'initializeObject', array('obj' => $obj), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'getFilters', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'isFiltersStateClean', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'hasFilters', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return $this->valueHolder9ac7f->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer23f97 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder9ac7f) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder9ac7f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder9ac7f->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, '__get', ['name' => $name], $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        if (isset(self::$publicProperties58d49[$name])) {
            return $this->valueHolder9ac7f->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder9ac7f;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder9ac7f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder9ac7f;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder9ac7f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, '__isset', array('name' => $name), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder9ac7f;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder9ac7f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, '__unset', array('name' => $name), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder9ac7f;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder9ac7f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, '__clone', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        $this->valueHolder9ac7f = clone $this->valueHolder9ac7f;
    }

    public function __sleep()
    {
        $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, '__sleep', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;

        return array('valueHolder9ac7f');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer23f97 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer23f97;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer23f97 && ($this->initializer23f97->__invoke($valueHolder9ac7f, $this, 'initializeProxy', array(), $this->initializer23f97) || 1) && $this->valueHolder9ac7f = $valueHolder9ac7f;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder9ac7f;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder9ac7f;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
