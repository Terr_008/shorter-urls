<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* url/index.html.twig */
class __TwigTemplate_9810807f7bdadba92686e7945a5a14e104446af47bd2b639fe51c2b3a81eda0f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "url/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "url/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "url/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello UrlController!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <style>
        .example-wrapper {
            margin: 1em auto;
            max-width: 800px;
            width: 95%;
            font: 18px/1.5 sans-serif;
        }

        .example-wrapper code {
            background: #F5F5F5;
            padding: 2px 6px;
        }

        table {
            text-align: left;
            position: relative;
            border-collapse: collapse;
            background-color: #f6f6f6;
        }

        /* Spacing */
        td, th {
            border: 1px solid #999;
            padding: 20px;
        }

        th {
            background: brown;
            color: white;
            border-radius: 0;
            position: sticky;
            top: 0;
            padding: 10px;
        }

        .primary {
            background-color: #000000
        }

        tfoot > tr {
            background: black;
            color: white;
        }

        tbody > tr:hover {
            background-color: #ffc107;
        }

    </style>

    ";
        // line 57
        echo "    ";
        // line 58
        echo "
    ";
        // line 60
        echo "    ";
        // line 61
        echo "    ";
        // line 62
        echo "    ";
        // line 63
        echo "    ";
        // line 64
        echo "    ";
        // line 65
        echo "
    ";
        // line 67
        echo "
    ";
        // line 68
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 68, $this->source); })()), 'form_start');
        echo "
    <div class=\"example-wrapper\">
        ";
        // line 70
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 70, $this->source); })()), 'errors');
        echo "
    </div>

    <div class=\"row\">
        <div class=\"col\">
            ";
        // line 75
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 75, $this->source); })()), "longUrl", [], "any", false, false, false, 75), 'row');
        echo "
            ";
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 76, $this->source); })()), "startTime", [], "any", false, false, false, 76), 'row', ["label" => "Start time"]);
        echo "
            ";
        // line 77
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 77, $this->source); })()), "endTime", [], "any", false, false, false, 77), 'row', ["label" => "End time"]);
        echo "
        </div>
    </div>
    <button type=\"submit\" class=\"button\">Отправить</button>
    ";
        // line 81
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 81, $this->source); })()), 'form_end');
        echo "

    <table border=\"1\">
        <caption>Urls</caption>
        <tr>
            <th>Long url</th>
            <th>Short url</th>
            <th>Views</th>
            <th>Start time</th>
            <th>End time</th>
        </tr>
        ";
        // line 92
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["urls"]) || array_key_exists("urls", $context) ? $context["urls"] : (function () { throw new RuntimeError('Variable "urls" does not exist.', 92, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["url"]) {
            // line 93
            echo "            <tr>
                <td><a href=\"";
            // line 94
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["url"], "longUrl", [], "any", false, false, false, 94), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["url"], "longUrl", [], "any", false, false, false, 94), "html", null, true);
            echo "</a></td>
                <td><a href=\"localhost:8000/";
            // line 95
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["url"], "shortUrl", [], "any", false, false, false, 95), "html", null, true);
            echo "\">localhost:8000/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["url"], "shortUrl", [], "any", false, false, false, 95), "html", null, true);
            echo "</a></td>
                <td>";
            // line 96
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["url"], "views", [], "any", false, false, false, 96), "html", null, true);
            echo "</td>
                <td>";
            // line 97
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["url"], "startTime", [], "any", false, false, false, 97), "H-i d.m.Y"), "html", null, true);
            echo "</td>
                <td>";
            // line 98
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["url"], "endTime", [], "any", false, false, false, 98), "H-i d.m.Y"), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['url'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 101
        echo "    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "url/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 101,  230 => 98,  226 => 97,  222 => 96,  216 => 95,  210 => 94,  207 => 93,  203 => 92,  189 => 81,  182 => 77,  178 => 76,  174 => 75,  166 => 70,  161 => 68,  158 => 67,  155 => 65,  153 => 64,  151 => 63,  149 => 62,  147 => 61,  145 => 60,  142 => 58,  140 => 57,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello UrlController!{% endblock %}

{% block body %}
    <style>
        .example-wrapper {
            margin: 1em auto;
            max-width: 800px;
            width: 95%;
            font: 18px/1.5 sans-serif;
        }

        .example-wrapper code {
            background: #F5F5F5;
            padding: 2px 6px;
        }

        table {
            text-align: left;
            position: relative;
            border-collapse: collapse;
            background-color: #f6f6f6;
        }

        /* Spacing */
        td, th {
            border: 1px solid #999;
            padding: 20px;
        }

        th {
            background: brown;
            color: white;
            border-radius: 0;
            position: sticky;
            top: 0;
            padding: 10px;
        }

        .primary {
            background-color: #000000
        }

        tfoot > tr {
            background: black;
            color: white;
        }

        tbody > tr:hover {
            background-color: #ffc107;
        }

    </style>

    {# <div class=\"example-wrapper\"> #}
    {# <h1>Hello {{ controller_name }}! ✅</h1> #}

    {# This friendly message is coming from: #}
    {# <ul> #}
    {# <li>Your controller at <code><a href=\"{{ 'C:/xamppNew/htdocs/testTask/src/Controller/UrlController.php'|file_link(0) }}\">src/Controller/UrlController.php</a></code></li> #}
    {# <li>Your template at <code><a href=\"{{ 'C:/xamppNew/htdocs/testTask/templates/url/index.html.twig'|file_link(0) }}\">templates/url/index.html.twig</a></code></li> #}
    {# </ul> #}
    {# </div> #}

    {# {{ form(form) }} #}

    {{ form_start(form) }}
    <div class=\"example-wrapper\">
        {{ form_errors(form) }}
    </div>

    <div class=\"row\">
        <div class=\"col\">
            {{ form_row(form.longUrl) }}
            {{ form_row(form.startTime,{'label': 'Start time'}) }}
            {{ form_row(form.endTime,{'label': 'End time'}) }}
        </div>
    </div>
    <button type=\"submit\" class=\"button\">Отправить</button>
    {{ form_end(form) }}

    <table border=\"1\">
        <caption>Urls</caption>
        <tr>
            <th>Long url</th>
            <th>Short url</th>
            <th>Views</th>
            <th>Start time</th>
            <th>End time</th>
        </tr>
        {% for url in urls %}
            <tr>
                <td><a href=\"{{ url.longUrl }}\">{{ url.longUrl }}</a></td>
                <td><a href=\"localhost:8000/{{ url.shortUrl }}\">localhost:8000/{{ url.shortUrl }}</a></td>
                <td>{{ url.views }}</td>
                <td>{{ url.startTime|date('H-i d.m.Y') }}</td>
                <td>{{ url.endTime|date('H-i d.m.Y') }}</td>
            </tr>
        {% endfor %}
    </table>
{% endblock %}
", "url/index.html.twig", "C:\\xamppNew\\htdocs\\shorter-urls\\templates\\url\\index.html.twig");
    }
}
