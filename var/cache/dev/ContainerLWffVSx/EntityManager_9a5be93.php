<?php

namespace ContainerLWffVSx;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHoldera35b2 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer425fa = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesa118b = [
        
    ];

    public function getConnection()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getConnection', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getMetadataFactory', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getExpressionBuilder', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'beginTransaction', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getCache', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getCache();
    }

    public function transactional($func)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'transactional', array('func' => $func), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'wrapInTransaction', array('func' => $func), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'commit', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->commit();
    }

    public function rollback()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'rollback', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getClassMetadata', array('className' => $className), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'createQuery', array('dql' => $dql), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'createNamedQuery', array('name' => $name), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'createQueryBuilder', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'flush', array('entity' => $entity), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'clear', array('entityName' => $entityName), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->clear($entityName);
    }

    public function close()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'close', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->close();
    }

    public function persist($entity)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'persist', array('entity' => $entity), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'remove', array('entity' => $entity), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'refresh', array('entity' => $entity), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'detach', array('entity' => $entity), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'merge', array('entity' => $entity), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getRepository', array('entityName' => $entityName), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'contains', array('entity' => $entity), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getEventManager', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getConfiguration', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'isOpen', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getUnitOfWork', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getProxyFactory', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'initializeObject', array('obj' => $obj), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'getFilters', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'isFiltersStateClean', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'hasFilters', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return $this->valueHoldera35b2->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer425fa = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHoldera35b2) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHoldera35b2 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHoldera35b2->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, '__get', ['name' => $name], $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        if (isset(self::$publicPropertiesa118b[$name])) {
            return $this->valueHoldera35b2->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera35b2;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera35b2;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera35b2;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera35b2;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, '__isset', array('name' => $name), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera35b2;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldera35b2;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, '__unset', array('name' => $name), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera35b2;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldera35b2;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, '__clone', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        $this->valueHoldera35b2 = clone $this->valueHoldera35b2;
    }

    public function __sleep()
    {
        $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, '__sleep', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;

        return array('valueHoldera35b2');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer425fa = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer425fa;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer425fa && ($this->initializer425fa->__invoke($valueHoldera35b2, $this, 'initializeProxy', array(), $this->initializer425fa) || 1) && $this->valueHoldera35b2 = $valueHoldera35b2;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldera35b2;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHoldera35b2;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
