<?php

namespace App\Controller;

use App\Entity\Url;
use App\Repository\UrlRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UrlRedirectController extends AbstractController
{
    /**
     * @Route("/{slug}", name="url_redirect")
     */
    public function index(string $slug, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $url = $entityManager->getRepository(Url::class)->findOneByShortUrl($slug);

        if (!$url) {
            throw $this->createNotFoundException(
                'No url found for short url ' . $slug
            );
        }

        if ($url->getStartTime() <= new \DateTime("now") && $url->getEndTime() > new \DateTime("now")) {
            $url->setViews($url->getViews() + 1);
            $entityManager->flush();

            return $this->redirect($url->getLongUrl());
        } elseif ($url->getStartTime() > new \DateTime("now")) {
            return $this->render('url_redirect/index.html.twig', [
                'message' => ' This link is not working yet.',
            ]);
        } elseif ($url->getEndTime() < new \DateTime("now")) {
            return $this->render('url_redirect/index.html.twig', [
                'message' => 'This link has expired.',
            ]);
        }

        return $this->render('url_redirect/index.html.twig', [
            'message' => 'Error',
        ]);
    }
}
