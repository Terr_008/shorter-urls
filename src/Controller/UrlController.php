<?php

namespace App\Controller;

use App\Entity\Url;
use App\Form\UrlType;
use App\Repository\UrlRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UrlController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, ManagerRegistry $doctrine, UrlRepository $urlRepository): Response
    {
        $url = new Url();

        $form = $this->createForm(UrlType::class, $url);

        $urls = $urlRepository->findAll();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $doctrine->getManager();

            $url->setLongUrl($form['longUrl']->getData());
            $url->setShortUrl($this->randomString(5));
            $url->setStartTime($form['startTime']->getData());
            $url->setEndTime($form['endTime']->getData());

            $entityManager->persist($url);

            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('url/index.html.twig', [
            'form' => $form->createView(),
            'urls' => $urls
        ]);
    }

    public function randomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
