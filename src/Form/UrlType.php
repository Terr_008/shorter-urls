<?php

namespace App\Form;

use App\Entity\Url;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UrlType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('longUrl')
            ->add('startTime', DateTimeType::class, [
                'input' => 'datetime',
                'widget' => 'choice',
                'data' => new \DateTime("now"),
            ])
            ->add('endTime', DateTimeType::class, [
                'input' => 'datetime',
                'widget' => 'choice',
                'data' => new \DateTime("now")
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Url::class,
        ]);
    }
}
